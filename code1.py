from Tkinter import*
import random
import math
import copy


def cost2(polygon):

	""" cost function is area of the parkable area as 
	defined in the poster"""

	#input polygon is already closed

	if (len(polygon)<3 or not(polygon[-1]==polygon[0])):
		print 'Err#235A: bad polygon ', polygon
		return 0

	roadWidth=3
	carLength=5
	stripWidth=roadWidth+2*carLength
	parkableArea=0

	# calculate the total length of path.
	totalLen=0	
	minAngleThreshold=30
	p=polygon
	acuteAngleCount=0
	for i in xrange(len(polygon)-1):

		# LS1= ith point to i+1th ( the one about to measure)
		# LS2 =i-1th point to ith point ( the prev. iteration measurement)
		if i>1:
			
			angleAti= getAngleBetweenPoints([p[i-1],p[i],p[i+1]])

			if math.degrees(angleAti)<minAngleThreshold:
				acuteAngleCount+=1
				continue
		# print 'added'

		totalLen+=distBetween(polygon[i],polygon[i+1])

	# gross area 
	parkableArea += (totalLen*stripWidth)

	#for every intersection remove two rhombuses.
	#list of points to list of line segments
	listOfLS=[]	
	for i in xrange(len(polygon)-1):
		listOfLS.append([polygon[i],polygon[i+1]])

	intersectCount=0
	for i in xrange(len(listOfLS)):
		for j in xrange(i+1,len(listOfLS)):
			if hasLSIntersect(listOfLS[i],listOfLS[j]):
				intersectCount+=1

	intersectCount=intersectCount

	#area of rhombus = stripWidth**2 
	aRhom=stripWidth**2
	parkableArea -= 2*aRhom*intersectCount
	# print intersectCount,'intersections found'

	# for every angle remove the quadrilateral twice.
	p=copy.deepcopy(polygon) # pop is necessary else it gets angle
	 # between tow same points and throws a dividion by zero error
	p.pop()


	#get angle between every three points.
	for i in xrange(len(p)):

		# avoid out of range error at the ends..
		
		k=(i+1)%len(p)
		if (p[i-1]==p[i] or p[i]==p[k] or p[i-1]==p[k]):
			continue

		angleAti= getAngleBetweenPoints([p[i-1],p[i],p[k]])

		angleAti=angleAti/2

		if angleAti <=0 or math.tan(angleAti)==0:
			print 'Err#7767: -ve angle alert!!',
			# print 'i is equal to ',i
			# print ([p[i-1],p[i],p[k]])
			# print polygon
			# print ((stripWidth/2.0)**2)/math.tan(angleAti)
			continue

		try:
			# get area of that corner small polygon
			aCorner= ((stripWidth/2.0)**2)/math.tan(angleAti)

		except:
			continue

		if aCorner<0:
			# print ([p[i-1],p[i],p[k]])
			print aCorner, angleAti, 'Err#345DF:this must not be negative'

		#remove that twice from the gross area
		parkableArea -= 2*aCorner

	# obtained the total gross parkable area
	
	return parkableArea

def getRandomPoint():
	"""returns a random (x,y) point. the poits are multiples of 5
	max till 500"""


	# rows=canvas.data.rows
	# cols=canvas.data.cols

	x=random.randint(0,100)
	y=random.randint(0,100)

	return (x*5,y*5)

def mousePressed(canvas,event):
	pass

def keyPressed(canvas,event):
	key= event.keysym
	print key
	if key=='r':
		init(canvas)
	elif key=='p':
		canvas.data.end=True
	elif key=='u':
		canvas.data.numOfVertices+=1
		canvas.data.polygon=generateRandomPolygon(canvas.data.numOfVertices)
	elif key=='j':
		canvas.data.numOfVertices-=1
		canvas.data.polygon=generateRandomPolygon(canvas.data.numOfVertices)

def initCounters(canvas):
	canvas.data.acuteAngleCount=0
	canvas.data.intersectCount=0

def timerFired(canvas):
	redrawAll(canvas)
	delay=2000

	doGenetics(canvas)

	if not(canvas.data.end):
		canvas.after(delay, lambda : timerFired(canvas))

def redrawAll(canvas):
	# draw the stats Center
	canvas.delete(ALL)
	drawDots(canvas)
	drawStatsCenter(canvas)
	
def drawStatsCenter(canvas):
	canW= canvas.data.canWidth
	canH= canvas.data.canHeight
	w=canvas.data.statsCenterWidth
	canvas.create_rectangle(canW - w, 0, canW,canH,fill='gray',width=0)
	canvas.create_rectangle(0,0,canW - w,canH,fill=None,width=1)

	canvas.create_text(canW,0,text='cost= \n %0.3f'%cost(canvas),anchor=NE)
	
	canvas.create_text(canW,50,text='acute Angles = %d'%canvas.data.acuteAngleCount,anchor=NE)
	canvas.create_text(canW,100,text='intersections = %d'%canvas.data.intersectCount,anchor=NE)

def generateRandomPolygon(numOfVertices):
	""" generates a random polygon with its vertices as 
	number of vertices provided. first and last vertex will be 
	same denoting closed polygon """

	polygon=[]
	for i in xrange(numOfVertices):
		polygon.append(getRandomPoint())



	polygon.append(polygon[0]) # important - we shall use 
	# all closed polygon in the entire code.. 

	
	return polygon

def drawDots(canvas):
	for i in xrange(0,500,5):
		for j in xrange(0,500,5):
			canvas.create_line(i,j,i+1,j,fill=None)

	canvas.create_polygon(canvas.data.polygon,fill='',outline='black',width=3)

def getAngleBetweenPoints(threePointList):
	#using law of cosines
	# c**2 = a**2 + b**2 - 2*a*b* cos@
	a=distBetween(threePointList[0],threePointList[1])
	b=distBetween(threePointList[1],threePointList[2])
	c=distBetween(threePointList[2],threePointList[0])

	if a==0 or b==0 or c==0:
		return 0



	try:
		angle = math.acos ((a**2 +b**2 -c**2) / (2.0*a*b ))
	except:
		print (a**2 +b**2 -c**2),  (2.0*a*b )
		print 'a,b,c',a,b,c
		return math.tan(math.radians(90))

	return angle

def distBetween(point1,point2):
	if not(type(point1) == tuple and type(point2)==tuple and\
	 len(point1)==2 and len(point1)==2):
		print 'Err#001: No List input and values are %r,%r'%(point1,point2)
		return 0
	x1,y1=point1[0],point1[1]
	x2,y2=point2[0],point2[1]

	return ((x1-x2)**2 + (y1-y2)**2)**0.5

def cost(canvas):
	""" cost function is area of the parkable area as 
	defined in the poster"""
	#input polygon is already closed

	polygon=canvas.data.polygon

	initCounters(canvas)

	if (len(polygon)<3 or not(polygon[-1]==polygon[0])):
		print 'Err#235A: bad polygon ', polygon
		return 0

	roadWidth=3
	carLength=5
	stripWidth=roadWidth+2*carLength
	parkableArea=0

	# calculate the total length of path.
	totalLen=0	
	minAngleThreshold=30
	p=polygon
	canvas.data.acuteAngleCount=0
	for i in xrange(len(polygon)-1):

		# LS1= ith point to i+1th ( the one about to measure)
		# LS2 =i-1th point to ith point ( the prev. iteration measurement)
		if i>1:
			angleAti= getAngleBetweenPoints([p[i-1],p[i],p[i+1]])
			if math.degrees(angleAti)<minAngleThreshold:
				canvas.data.acuteAngleCount+=1
				continue
		# print 'added'

		totalLen+=distBetween(polygon[i],polygon[i+1])

		




	# gross area 
	parkableArea += (totalLen*stripWidth)

	#for every intersection remove two rhombuses.
	#list of points to list of line segments
	listOfLS=[]	
	for i in xrange(len(polygon)-1):
		listOfLS.append([polygon[i],polygon[i+1]])

	intersectCount=0
	for i in xrange(len(listOfLS)):
		for j in xrange(i+1,len(listOfLS)):
			if hasLSIntersect(listOfLS[i],listOfLS[j]):
				intersectCount+=1

	canvas.data.intersectCount=intersectCount

	#area of rhombus = stripWidth**2 
	aRhom=stripWidth**2
	parkableArea -= 2*aRhom*intersectCount
	

	# for every angle remove the quadrilateral twice.
	p=copy.deepcopy(polygon) # pop is necessary else it gets angle
	 # between tow same points and throws a dividion by zero error
	p.pop()


	#get angle between every three points.
	for i in xrange(len(p)):
		# avoid out of range error at the ends..
		
		k=(i+1)%len(p)
		angleAti= getAngleBetweenPoints([p[i-1],p[i],p[k]])
		angleAti=angleAti/2

		if angleAti <0:
			print 'Err#7767: -ve angle alert!!',
			# print ((stripWidth/2.0)**2)/math.tan(angleAti)

		# get area of that corner small polygon
		try:
			aCorner= ((stripWidth/2.0)**2)/math.tan(angleAti)
		except:
			print 'zero angle'
			aCorner=0

		if aCorner<0:
			print canvas.data.polygon
			print aCorner, angleAti, 'Err#345DF:this must not be negative'

		#remove that twice from the gross area
		parkableArea -= 2*aCorner

	# obtained the total gross parkable area
	
	return parkableArea

# returns intersection points of two line segments
# called as a instance method.
def intersectionOfLines(lineSegment1,lineSegment2):
	""" takes two linesegments as [(x1,y1),(x2,y2)] and 
	returns intersection as a point (x,y). 
	returns None for parallel lines."""

	#basic input checks
	if not(type(lineSegment1)==list and type(lineSegment2)==list and 
		len(lineSegment1)==2 and len(lineSegment2)==2):
		print 'Err#983F: inputs not line segments'
		print 'ls1 = ',lineSegment1
		print 'ls2 = ',lineSegment2
		return None

	for i in xrange(1):
		if not type(lineSegment1[i])==tuple or not type(lineSegment2[i])==tuple:
			print "Err:#9978: no tuple points in lineSegments"
			print 'ls1 = ',lineSegment1
			print 'ls2 = ',lineSegment2
			return None

		if not len(lineSegment1[i])==2 or not len(lineSegment2[i])==2:
			print "Err:#9978: tuple points in lineSegments have len >2"
			print 'ls1 = ',lineSegment1
			print 'ls2 = ',lineSegment2
			return None


	[(x1,y1),(x2,y2)]=lineSegment1
	[(x3,y3),(x4,y4)]=lineSegment2
	# vertical lines
	if (x1==x2 and x3==x4):
		return None
	elif(x1==x2):
		a2 = (y4-y3)/float(x4-x3)
		b2 = y3 - a2*float(x3)
		# y=a2 x + b2 is the non vertical line
		#@ x=x1  
		return [x1,float(a2*x1 + b2)]

	elif (x3==x4):
		a1 = (y2-y1)/float(x2-x1)
		b1 = y1 - a1*float(x1)

		return [x3,float(a1*x3 + b1)]

	else:
		a1 = (y2-y1)/float(x2-x1)
		b1 = y1 - a1*float(x1) 
		a2 = (y4-y3)/float(x4-x3)
		b2 = y3 - a2*float(x3)
	if a1==a2 : return None

	x0 = -(b1-b2)/(a1-a2)
	y0= a1*x0 + b1
	return (x0,y0)

def hasLSIntersect(ls1,ls2):
	""" takes two linesegments and returns if the line
	segments have an intersection. It accounts only
	intersections on both line segments."""

	# input checks handled in intersection of lines

	# before accessing iPoint check for none signifying parallel lines
	iPoint=intersectionOfLines(ls1,ls2)
	if iPoint==None:
		return False

	[(x1,y1),(x2,y2)]=ls1
	[(x3,y3),(x4,y4)]=ls2

	# if the point lies in the range on both of the lines then True:
	if iPoint[0] >= min(x1,x2) and iPoint[0]<= max(x1,x2) and\
		iPoint[0]>= min (x3,x4) and iPoint[0] <= max(x3,x4):
		return True

	return False

def testCost():
	# jsut a random list
	# print cost([(410, 20), (250, 195), (50, 450), (160, 185), (340, 360),
	 # (410, 20)])

	# function for super packing;
	
	class struct():pass
	canvas=struct()
	canvas.data=struct()
	canvas.data.polygon=[(340, 140), (325, 395), (270, 155), (100, 220),
	 (135, 40), (120, 165), (150, 195), (175, 400), (215, 255),
	  (145, 415), (215, 340), (285, 265), (340, 140)]
	print cost(canvas)

def init(canvas):
	canvas.data.polygon=([(6.5, 6.5), (6.5, 493.5), (20.0, 493.5), 
		(20.0, 6.5), (33.5, 6.5), (33.5, 493.5), (47.0, 493.5), 
		(47.0, 6.5), (60.5, 6.5), (60.5, 493.5), (74.0, 493.5),
		(74.0, 6.5), (87.5, 6.5), (87.5, 493.5), (101.0, 493.5), 
		(101.0, 6.5), (114.5, 6.5), (114.5, 493.5), (128.0, 493.5),
		(128.0, 6.5), (141.5, 6.5), (141.5, 493.5), (155.0, 493.5), 
		(155.0, 6.5), (168.5, 6.5), (168.5, 493.5), (182.0, 493.5),
		(182.0, 6.5), (195.5, 6.5), (195.5, 493.5), (209.0, 493.5),
		(209.0, 6.5), (222.5, 6.5), (222.5, 493.5), (236.0, 493.5),
		(236.0, 6.5), (249.5, 6.5), (249.5, 493.5), (263.0, 493.5),
		(263.0, 6.5), (276.5, 6.5), (276.5, 493.5), (290.0, 493.5), 
		(290.0, 6.5), (303.5, 6.5), (303.5, 493.5), (317.0, 493.5), 
		(317.0, 6.5), (330.5, 6.5), (330.5, 493.5), (344.0, 493.5), 
		(344.0, 6.5), (357.5, 6.5), (357.5, 493.5), (371.0, 493.5), 
		(371.0, 6.5), (384.5, 6.5), (384.5, 493.5), (398.0, 493.5), 
		(398.0, 6.5), (411.5, 6.5), (411.5, 493.5), (425.0, 493.5), 
		(425.0, 6.5), (438.5, 6.5), (438.5, 493.5), (452.0, 493.5), 
		(452.0, 6.5), (465.5, 6.5), (465.5, 493.5), (479.0, 493.5), 
		(479.0, 6.5), (492.5, 6.5), (492.5, 493.5),(6.5, 6.5)])
	canvas.data.numOfVertices=10
	# canvas.data.polygon=generateRandomPolygon(canvas.data.numOfVertices)
	canvas.data.end=False
	initGenetics(canvas)

def doMutation(polygon):
	""" adds or subtracts a point with equal probablity"""
	# shuld not subtract if len of plygon is 3 ( it would need at least 3 points)
	# return polygon
	polygon=copy.deepcopy(polygon)

	randIndex=random.randint(0,len(polygon)-1)
	randNum=random.randint(0,9)

	if len(polygon)<3:
		randNum=1 # only adding a point in case the length of polygon is less than 1

	if randNum==1:

		# try to add a point and make a valid polygon 
		# for 10 times. after that give up
		counter=0
		while(counter<10):
			# adding 
			randPoint=getRandomPoint()
			polygon2=polygon[:randIndex]+[randPoint]+polygon[randIndex:]
			if isValidPolygon(polygon2):
				polygon=polygon2
				break
			counter+=1

	else:

		# try to pop a point and make a valid polygon 
		# for 10 times. after that give up
		counter=0
		while(counter<10):
			polygon2=copy.deepcopy(polygon)
			polygon2.pop(randIndex)
			if isValidPolygon(polygon2):
				polygon=polygon2
				break
			counter+=1
		

	if polygon[0]==polygon[-1]:
		return polygon
	else:
		return polygon+[polygon[0]]

def generateRandomIndivs(canvas):

	#generate initial population 
	initPopList=[]
	for i in xrange(canvas.data.popLimit):
		k=random.randint(canvas.data.initMinPoints,canvas.data.initMaxPoints)
		initPopList.append(generateRandomPolygon(k))


	costIndivList=[]
	# [cost,indiv] items are populaed in this list
	for i in xrange(len(initPopList)):
		costIndivList.append([cost2(initPopList[i]),initPopList[i]])

	canvas.data.costIndivList=sorted(costIndivList)[::-1]
	canvas.data.polygon=canvas.data.costIndivList[1][1]
	canvas.data.polygon=canvas.data.polygon + [canvas.data.polygon[0]]
	canvas.data.polyCost=canvas.data.costIndivList[1][0]

def doGenetics(canvas):
	""" the genetic algorithm"""

	#parse the old list

	costIndivList=copy.deepcopy(canvas.data.costIndivList)

	# keep the top quarter best ones
	# do crossover to next quater
	# the last quarter mutates.

	
	print '***************************************************************\n'*5

	print 'genetics round started with ', costIndivList

	totalIndividuals=len(costIndivList)
	print costIndivList

	quarter=totalIndividuals/4
	newList=[]
	print 'number of totalIndividuals',totalIndividuals

	for i in xrange(totalIndividuals):
		print 'in loop1'

		if i <=quarter:
			newList.append(costIndivList[i])

		elif i<=quarter*2:
			# do crossover with index+quarter
			[indiv1,indiv2]=doCrossOver(costIndivList[i][1],costIndivList[i+quarter][1])
			newList.append([cost2(indiv1),indiv1])
			newList.append([cost2(indiv2),indiv2])
	
	for i in xrange(quarter):
		# here a max of 3 individual may be left untouched.
		mutatedIndiv= doMutation(costIndivList[i][1])

		newList.append([cost2(mutatedIndiv),mutatedIndiv])


	print 'newList generated in this stage is',newList

	canvas.data.costIndivList=sorted(newList)[::-1]

	try:
		
		canvas.data.polygon= canvas.data.costIndivList[0][1]


		if not canvas.data.polygon[0]==canvas.data.polygon[-1]:
			canvas.data.polygon=canvas.data.polygon + [canvas.data.polygon[0]]
		canvas.data.polyCost=canvas.data.costIndivList[1][0]
	except:
		print 'newList',newList
		print 'error occured'

def isValidPolygon(pointList):
	"""checks if the polygon is a circularPolygon"""
	pointList=copy.deepcopy(pointList)

	# remove the last point.. 
	pointList.pop()

	# checks if the pointlist at least has 3 points
	if len(pointList)<3:
		return False

	# check all the line segments intersection with
	for i in xrange(len(pointList)):
		ls1= [(pointList[i-2],pointList[i-1])]
		ls2=[(pointList[i-1],pointList[i])]
		if hasLSIntersect(ls1,ls2):
			return False

	return True

def initGenetics(canvas):

	# population at a point of time
	canvas.data.popLimit=8

	# initial max points in polygon
	canvas.data.initMaxPoints=12
	# initial min points polygon
	canvas.data.initMinPoints=11
	# a random number is generated  between these min and max 
	# the random polygon generaed shall have that many vertices.
	generateRandomIndivs(canvas)

def doCrossOver2(poly1,poly2):
	""" just calls doCrossOver2.. for 10 times
	and tries to pass only valid polygons"""

	poly1=copy.deepcopy(poly1)
	poly2=copy.deepcopy(poly2)
	counter=0
	while(counter<10):
		p1,p2=doCrossOver2(poly1,poly2)
		if isValidPolygon(p1) and isValidPolygon(p2):
			poly1,poly2=p1,p2
			break

	return[poly1,poly2]

def doCrossOver(poly1,poly2):
	""" takes to polygons and returns two polygons in a list"""
	return [poly1,poly2]
	#loop and get a solution till it gives a valid solution
	checkBool=True
	while(checkBool):
		checkBool=False
		randIndex1=random.randint(0,len(poly1)-1)
		randIndex2=random.randint(0,len(poly2)-1)
		newPoly1=poly1[:randIndex1]+poly2[randIndex2:]
		newPoly2=poly2[:randIndex2]+poly1[randIndex1:]
		if len(newPoly2)<3 or len(newPoly1)<3:
			checkBool=True


	if not(newPoly1[0]==newPoly1[-1]):
		newPoly1=newPoly1+[newPoly1[0]]
	if not(newPoly2[0]==newPoly2[-1]):
		newPoly2= newPoly1+[newPoly1[0]]


	return [newPoly1,newPoly2]

def run(rows,cols):
	"""main run function"""
	# create the root and the canvas
	root = Tk()
	cellWidthPixel=5
	statsCenterWidth=200
	width,height=cols*cellWidthPixel+statsCenterWidth,rows*cellWidthPixel
	canvas = Canvas(root, width=width, height=height)
	canvas.pack()
	root.resizable(width=0, height=0)
	# Set up canvas data
	class Struct: pass
	canvas.data = Struct()
	canvas.data.rows,canvas.data.cols=rows,cols
	canvas.data.canWidth,canvas.data.canHeight=width,height
	canvas.data.statsCenterWidth=statsCenterWidth

	#initialize with init()
	init(canvas)
	# bind
	root.bind("<Button-1>", lambda event: mousePressed(canvas, event))
	root.bind("<Key>", lambda event: keyPressed(canvas, event))
	#call the app
	timerFired(canvas) 
	root.mainloop() 

def testgetanglebetweenpoints():
	print getAngleBetweenPoints([(0,0),(1,0),(0,1)])
	print getAngleBetweenPoints([(240, 185), (220, 195), (430, 90)])



run(100,100)
# testCost()
# testgetanglebetweenpoints()

# check intersection function.
# print hasLSIntersect([(20.0, 493.5), (20.0, 6.5)],[(492.5, 493.5),(6.5, 6.5)])


def generateGoodFitPoly():
	goodfitpoly=[]

	def drange(start, stop, step):
		r = start
		while r < stop:
			yield r
			r += step
	count=0
	for i in drange(6.5,500,13.5):
		if count%2==0:
			goodfitpoly.append((i,6.5))
			goodfitpoly.append((i,500- 6.5))
		else:
			goodfitpoly.append((i,500- 6.5))
			goodfitpoly.append((i,6.5))
		count+=1

	return goodfitpoly
